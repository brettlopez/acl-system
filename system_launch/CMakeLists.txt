cmake_minimum_required(VERSION 2.8.3)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -O3")
add_compile_options(-std=c++11) 
project(system_launch)
find_package(catkin REQUIRED COMPONENTS 
  roscpp
  rospy
  tf2
  rosbag
  roslib
  std_msgs 
  geometry_msgs
  acl_msgs
)


catkin_package(CATKIN_DEPENDS std_msgs geometry_msgs)

include_directories(${catkin_INCLUDE_DIRS})
add_executable(vicon_relay src/vicon_relay.cpp)
add_dependencies(vicon_relay ${catkin_EXPORTED_TARGETS})
target_link_libraries(vicon_relay ${catkin_LIBRARIES})
