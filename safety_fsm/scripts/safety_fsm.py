#!/usr/bin/env python
import rospy
from copy import deepcopy
from acl_msgs.msg import CommAge, QuadMode, QuadGoal, BoolStamped

class SafetyFSMNode():
	def __init__(self):
		self.node_name = rospy.get_name()
		self.pub_mode = rospy.Publisher("~mode",QuadMode,queue_size=1,latch=True)

		# Get all params from launch file
		self.safety_fsm_hz = rospy.get_param("~safety_fsm_hz",1)
		self.t_vicon1 = float(rospy.get_param("~safety_fsm_t_vicon1",1))
		self.t_vicon2 = float(rospy.get_param("~safety_fsm_t_vicon2",1))
		assert self.t_vicon1 <= self.t_vicon2

		self.start_trigger_debounce = rospy.Duration.from_sec(rospy.get_param("~start_trigger_debounce",0.05))

		# Initialize State Machine and publish first state
		self.state = QuadMode()
		self.state.mode = self.state.MODE_ON_GROUND_DO_NOT_TAKEOFF
		self.previousState = QuadMode()
		self.previousState.mode = None
		self.publishMode()

		self.did_cut_power = True
		self.dt_vicon = 0.0
		self.start_trigger_received = False
		self.last_trigger_received = BoolStamped()

		# Setup subscriber at the end of the initializer
		self.sub_comm_ages = rospy.Subscriber("~ages",CommAge,self.cbNewAgeMsg)
		self.sub_goal = rospy.Subscriber("~goal",QuadGoal,self.cbNewGoalMsg)
		self.sub_start_trigger = rospy.Subscriber("~start_trigger",BoolStamped,self.cbStartTrigger)

		# Setup timer for state machine updates
		self.safety_fsm_period = 1./self.safety_fsm_hz
		self.timer = rospy.Timer(rospy.Duration.from_sec(self.safety_fsm_period),self.cbStateUpdate)

	def cbStateUpdate(self,event):
		self.updateState()

	def cbStartTrigger(self,msg):
		t = msg.header.stamp - self.last_trigger_received.header.stamp
		rospy.loginfo("t: %s, debounce: %s" %(t,self.start_trigger_debounce))
		self.last_trigger_received = deepcopy(msg)
		if t > self.start_trigger_debounce:
			self.start_trigger_received = not self.start_trigger_received

	def updateState(self):
		# rospy.loginfo("[%s] dt_vicon: %s dt_waypt: %s mode: %s" %(self.node_name,self.dt_vicon,self.dt_goal,self.state.mode))
		self.previousState = deepcopy(self.state)
		if self.did_cut_power and (self.state.mode != self.state.MODE_ON_GROUND_DO_NOT_TAKEOFF) and (self.state.mode != self.state.MODE_ON_GROUND_READY_FOR_TAKEOFF):
			self.start_trigger_received = False
			self.state.mode = self.state.MODE_ON_GROUND_DO_NOT_TAKEOFF
		elif self.state.mode == self.state.MODE_WAYPOINT:
			if self.dt_vicon > self.t_vicon1:
				self.state.mode = self.state.MODE_ZERO_VEL
		elif self.state.mode == self.state.MODE_ZERO_VEL:
			if self.dt_vicon < self.t_vicon1:
				self.state.mode = self.state.MODE_WAYPOINT
			elif self.dt_vicon > self.t_vicon2:
				self.state.mode = self.state.MODE_DESCEND
		elif self.state.mode == self.state.MODE_ON_GROUND_DO_NOT_TAKEOFF: 
			# Special absorbing state, needed to avoid situ where accidental blocking of vicon dots before takeoff would trigger zero velocity/landing maneuvers
			if self.dt_vicon < self.t_vicon1:
				if self.start_trigger_received:
					self.state.mode = self.state.MODE_ON_GROUND_READY_FOR_TAKEOFF
			else: # no vicon, so ignore the trigger
				self.start_trigger_received = False
		elif self.state.mode == self.state.MODE_ON_GROUND_READY_FOR_TAKEOFF:
			if self.dt_vicon > self.t_vicon1 or self.start_trigger_received is False:
				self.state.mode = self.state.MODE_ON_GROUND_DO_NOT_TAKEOFF
				self.start_trigger_received = False
			elif not self.did_cut_power:
				self.state.mode = self.state.MODE_WAYPOINT

		self.publishMode()

	def publishMode(self):
		if self.previousState.mode != self.state.mode:
			self.pub_mode.publish(self.state)

	def cbNewAgeMsg(self,age_msg):
		self.state.header.stamp = age_msg.header.stamp
		self.dt_vicon = age_msg.vicon_age_secs

	def cbNewGoalMsg(self,msg):
		self.did_cut_power = deepcopy(msg.cut_power)

	def on_shutdown(self):
		rospy.loginfo("[%s] Shutting down." %(self.node_name))

if __name__ == '__main__':
    rospy.init_node('safety_fsm_node', anonymous=False)
    node = SafetyFSMNode()
    rospy.on_shutdown(node.on_shutdown)
    rospy.spin()