echo "Checking repo status."


cd ~/acl_ws/src/acl-system
git status
git branch

echo "Press [ENTER] to confirm all changes are committed and you're on the right branch..."
read 

cd ~/acl_ws/src/acl_control
git status
git branch

echo "Press [ENTER] to confirm all changes are committed and you're on the right branch..."
read 


echo "ACL stack configuration confirmed, ready for testing."

