#!/usr/bin/env bash
echo "Activating workspace"
source ~/catkin_ws/devel/setup.bash
# source ~/boeing _ws/devel/setup.bash
echo "Setup ROS_HOSTNAME"
export ROS_HOSTNAME=$HOSTNAME.local
exec "$@" #Passes arguments. Need this for ROS remote launching to work.
