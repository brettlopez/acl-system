'''
Created on Nov 21, 2014

@author: brettlopez
'''

import socket as sk
import pickle as pick
import time as t
import select

from collections import namedtuple


CommData = namedtuple("CommData", "id data timestamp")

class comm(object):

    def __init__(self,host,port):
        # Initialize the comm with host and port
        self.UDPSock = sk.socket(sk.AF_INET,sk.SOCK_DGRAM)
        self.host = host
        self.port = port
        print '[Protocol.comm] initialized with host %s port %s' %(self.host,self.port) 

    def bind_port(self):
        # Bind to the address
        print '[Protocol.comm] Binding: %s %s' %(self.host,self.port)  
        self.UDPSock.bind((self.host,self.port))
        
    def deliver(self,msg_id,msg_data,msg_timestamp=None):      
        # Pickle data and deliver thourgh UDP

        # Set timestamp to deliver time if not provided
        if(msg_timestamp==None):
            msg_timestamp = t.time()

        # Pickle data using the nameedtuple CommData
        pick_data = pick.dumps(CommData(msg_id,msg_data,msg_timestamp))

        # Deliver thourgh UDP to the destination
        if(self.UDPSock.sendto(pick_data,(self.host,self.port))):
            pass
            # print "[Protocol.comm] Sending message through host %s port %s. Size %s" %(self.host,self.port,len(pick_data))

    def ready_to_read(self):
        # Return True if the socket is ready to read (still has msg in queue), False if not.
        socketlist = [self.UDPSock]
        readReadyList, writeReadyList, errorList = select.select(socketlist,[],[],0.0)
        if len(readReadyList)==0:
            # socket not ready to read, return None
            return False
        else:
            return True

    def accept(self,buf=4096):
        # Receive messages and return the unpickled data
        up_data = self.UDPSock.recv(buf)
        return pick.loads(up_data)
        # print "[Protocol.comm] Received message through host %s port %s" %(self.host,self.port)

    def accept_all(self,buf=4096):
        # Return a list of CommData in the socket (can be empty list)
        msgList = []
        while self.ready_to_read():
            msgList.append(self.accept(buf))
        return msgList

    def drain(self,buf=4096):
        # Drain the socket by keep recv until it's empty
        start_time = t.time()
        while 1:
            if self.ready_to_read():
                self.UDPSock.recv(buf)
            else:
                break
        print 'comm.drian took %s ms' %(1000*(t.time()-start_time))

    def close_port(self):
        # Close the port
        # print "[Protocol.comm] Closing host %s port %s" %(self.host,self.port)
        self.UDPSock.close()

